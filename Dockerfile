FROM php:8.2-apache

RUN a2enmod rewrite && \
    apt update && \
    apt install -y wget zip libzip-dev && \
    docker-php-ext-install pdo pdo_mysql bcmath zip sockets

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
COPY . /var/www/html
WORKDIR /var/www/html
