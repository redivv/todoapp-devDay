<?php

namespace acceptance;

use Tests\Support\AcceptanceTester;
use Tests\Support\Data\TaskTableConst;

class TaskTableCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->registerTestUser($I);
    }

    public function createNewTable(AcceptanceTester $I): void
    {
        $I->wantTo("Add a new table");

        $I->click('#addTableButton');
        $I->seeInPopup("Podaj nazwę tabelki");
        $I->typeInPopup(TaskTableConst::TEST_TABLE_NAME);
        $I->acceptPopup();
        $I->waitForElement('.tableLink.activeTable');
        $I->canSee(TaskTableConst::TEST_TABLE_NAME, '.tableLink');

        $I->seeInDatabase('tables', [
            'name' => TaskTableConst::TEST_TABLE_NAME,
            'user_id' => 1,
        ]);
    }
}
